// include guard
#ifndef __GAME_INCLUDED__
#define __GAME_INCLUDED__

#pragma region Includes
// Std. Includes
#include <iostream>
#include <cmath>
#include <map>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// Other Libs
#include <SOIL.h>
// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// Our includes
#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Light.h"
#pragma endregion


// Constants
const float							M_PI = 3.14159265358979323846;

class Game {

public:
	// Start the execution of the game
	void							StartGame();

private:
	// List of shaders in the game
	std::map<std::string, Shader*>	m_shaders;
	// List of model in the game
	std::map<std::string, Model*>	m_models;
	// List of lights in the game
	std::map<std::string, Light*>	m_lights;
	// Depth map of scene
	GLuint							depthMap;
	GLuint							depthMapFBO;
	/// SSS variables
	float							distorsionForSSS;
	float							scaleForSSS;
	float							powerForSSS;

	// Timer for the movement of the player
	float							timeValue;

	// Load all the necessary shaders
	void							loadShaders();
	// Load all the necessary models
	void							loadModels();
	// Load all the necessary lights 
	void							loadLights();

	// Draw the lights in the scene
	void							drawLights();
	// Draw all the models in the scene, excluding the model of the lights
	void							drawModels(bool isShadowMapRendering = false);

	// Draw specific models
	void							drawFern(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &projection, glm::mat4 &view, glm::mat4 &model);
	void							drawCrystal(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &lightSpaceMatrix, glm::mat4 &projection, glm::mat4 &view, glm::mat4 &model);
	void							drawPlayer(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &lightSpaceMatrix, glm::mat4 &projection, glm::mat4 &view);
	void							drawCave(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &lightSpaceMatrix, glm::mat4 &projection, glm::mat4 &view, glm::mat4 &model);

	// Setup the info of the lights for pbr
	void							setupShaderForPBR(Shader* shader);

	// Set uniform for blur: shader configuration
	void							setUniformForBlur();

	//Apply blur effect to the scene
	void							applyBlur(FrameBufferBlur &fbb);

	void							renderToDepthMap(const GLuint &depthMapFBO);
	void							setupFramebufferForShadowmap(GLuint& depthMapFBO, GLuint& depthMap);
};


// Function prototypes
void								key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void								mouse_callback(GLFWwindow* window, double xpos, double ypos);
void								scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void								moveCamera();
void								moveLight(glm::mat4* light_position, GLfloat deltaTime, GLfloat speedness);


// Variables for SSS
bool								modifySSSParam(float* distorsionForSSS, float* scaleForSSS, float* powerForSSS);


#endif __GAME_INCLUDED__