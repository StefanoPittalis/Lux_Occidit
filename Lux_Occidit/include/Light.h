// include guard
#ifndef __LIGHT_INCLUDED__
#define __LIGHT_INCLUDED__

#pragma region Includes
// Std. Includes
// External Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
// Our Includes
#include "Model.h"
#pragma endregion

// The kind of light we could be using
enum light_types {	pointLight, spotLight, directionalLight};

class Light {

public:

	// Model containing the mesh in the scene rappresenting the light
	Model*							modelOfLight;

	// Real position of the light source
	glm::vec3						getPosition();

	// Direction of the light, used only for spotLights
	glm::vec3						direction;

	// Type of the light
	light_types						typeOfLight;

	// Constructor
	Light(GLchar* path, light_types typeOfLight, glm::mat4 homogeneousMatrix = glm::mat4(0.0f), glm::vec3 direction = glm::vec3(0.0f));



private:

};

#endif  __LIGHT_INCLUDED__