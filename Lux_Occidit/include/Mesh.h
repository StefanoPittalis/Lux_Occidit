// include guard
#ifndef __MESH_INCLUDED__
#define __MESH_INCLUDED__

#pragma region Includes
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
// External Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/scene.h>'
// Our Includes
#include "Shader.h"  
#pragma endregion

typedef unsigned int uint;


struct Vertex {
	// Position
	glm::vec3 Position;
	// Normal
	glm::vec3 Normal;
	// TexCoords
	glm::vec2 TexCoords;
};

struct Texture {
	GLuint id;
	std::string type;
	aiString path;
};

// Struct for cook torrance material: ONLY ONE MATERIAL PER MODEL!
struct Material {
	unsigned int albedo = 0;
	unsigned int normal = 0;
	unsigned int metallic = 0;
	unsigned int roughness = 0;
	unsigned int ao = 0;
	unsigned int thickness = 0;
};

class Mesh {
	public:
		/*  Variables  */
		//Mesh Data
		std::vector<Vertex>				vertices;
		std::vector<GLuint>				indices;
		std::vector<Texture>			textures;
		aiMesh*							ai_mesh;

		//Render data
		GLuint							VAO, VBO, EBO;
		Material						material;
		
		/*  Functions  */
		// Constructor
		Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures,aiMesh* ai_mesh);

		// Render the mesh
		void Draw(Shader* shader);

		//Render the mesh with cook torrance pbr
		void DrawPBR(Shader* shader);

	private:
		/*  Variables  */
		int idMesh;
		static int numOfMeshes;
		
		/*  Functions    */
		// Initializes all the buffer objects/arrays
		void setupMesh();	
};

#endif  __MESH_INCLUDED__
