// include guard
#ifndef __MODEL_INCLUDED__
#define __MODEL_INCLUDED__

#pragma region Includes
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
// External Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SOIL.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
// Our Includes
#include "Mesh.h"  
#include "SkeletalAnimation/SkeletalModel.h"
#include "SkeletalAnimation/AssimpConverter.h"
#pragma endregion

GLint TextureFromFile(const char* path, std::string directory);

class Model
{
public:
	/*  Variables  */
	const aiScene*			scene;
	std::vector<Mesh>		meshes;

	// Position in scene
	glm::mat4				modelMatrix;

	// Animated model
	SA::SkeletalModel		AnimatedModel;
	
	// Struct for cook torrance material: ONLY ONE MATERIAL PER MODEL!
	Material				material;

	/*  Functions   */
	// Constructor, expects a filepath to a 3D model.
	Model(GLchar* path, glm::mat4 position = glm::mat4(0.0f));
	Model() {};

	// Draws the model, and thus all its meshes
	void Draw(Shader* shader);

	// Draws the model with cook torrance
	void DrawPBR(Shader* shader);

	// Play the animation, drawing the model
	void PlayAnimation(Shader shader, GLfloat deltaTime);

	//Fill the material field with correct special textures for cook torrance
	void SetUpMaterial(std::string albedo_path, std::string normal_path, std::string metallic_path, std::string roughness_path, std::string ao_path, std::string thickness_path = "");
	void SetUpMaterialAnimation(std::string albedo_path, std::string normal_path, std::string metallic_path, std::string roughness_path, std::string ao_path);

private:

	/*	Variables	*/
	//Path to directory of the model
	std::string					directory;

	// Textures already loaded
	std::vector<Texture>		textures_loaded;

	// Importer for the scene.
	Assimp::Importer			importer;

	/*  Functions   */
	// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
	void loadModel(std::string path);

	// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	void processNode(aiNode* node, const aiScene* scene);

	Mesh processMesh(aiMesh* mesh, const aiScene* scene);

	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
	
};

#endif __MODEL_INCLUDED__