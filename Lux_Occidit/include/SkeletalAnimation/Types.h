#pragma once

// Contains all the necessery OpenGL includes
#include <GL/glew.h> 
// GLM
#include "glm/glm.hpp"
#include "glm/ext.hpp"
// STD
#include <vector>
#include <string>
// Our Includes
#include "Shader.h" 
#include "Mesh.h" 


namespace SA
{
	struct sWeight
	{
		unsigned int VertexID;
		float Weight;
	};

	struct sBone
	{
		std::string Name;

		glm::mat4x4 NodeTransform;
		glm::mat4x4 OffsetMatrix; // T-Pose to local bone space
		glm::mat4x4 FinalTransformation;

		unsigned int NumWeights;
		sWeight* pWeights;

		unsigned int NumChildren;
		unsigned int* pChildren;
	};

	struct sAnimatedMesh
	{
		unsigned int NumVertices;

		glm::vec3* pVertices;
		glm::vec3* pNormals;

		glm::vec3* pTransformedVertices;
		glm::vec3* pTransformedNormals;

		unsigned int NumIndices;
		unsigned int* pIndices;

		unsigned int VAO, VBO, EBO;

		Material	material;

		std::vector<Vertex> vertices;
		std::vector<Texture> textures;

		sAnimatedMesh() {
			// Create buffers/arrays
			glGenVertexArrays(1, &this->VAO);
			glGenBuffers(1, &this->VBO);
			glGenBuffers(1, &this->EBO);
		}

		void Draw(Shader shader) {

			// Prepare the mesh
			setupMesh();

			// Bind appropriate textures
			GLuint diffuseNr = 1;
			GLuint specularNr = 1;
			for (GLuint i = 0; i < this->textures.size(); i++)
			{
				glActiveTexture(GL_TEXTURE0 + i); // Active proper texture unit before binding
												  // Retrieve texture number (the N in diffuse_textureN)
				std::stringstream ss;
				std::string number;
				std::string name = this->textures[i].type;
				if (name == "texture_diffuse")
					ss << diffuseNr++; // Transfer GLuint to stream
				else if (name == "texture_specular")
					ss << specularNr++; // Transfer GLuint to stream
				number = ss.str();
				// Now set the sampler to the correct texture unit
				glUniform1i(glGetUniformLocation(shader.Program, (name + number).c_str()), i);
				// And finally bind the texture
				glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
			}

			// Also set each mesh's shininess property to a default value (if you want you could extend this to another mesh property and possibly change this value)
			//glUniform1f(glGetUniformLocation(shader.Program, "material.shininess"), 16.0f);

			// Draw mesh
			glBindVertexArray(this->VAO);
			glDrawElements(GL_TRIANGLES, this->NumIndices, GL_UNSIGNED_INT, 0);
			glBindVertexArray(0);

			// Always good practice to set everything back to defaults once configured.
			for (GLuint i = 0; i < this->textures.size(); i++)
			{
				glActiveTexture(GL_TEXTURE0 + i);
				glBindTexture(GL_TEXTURE_2D, 0);
			}
		};

		void DrawPBR(Shader* shader) {

			//if they are all zero the material is not set
			if (!material.albedo && !material.normal && !material.ao && !material.metallic && !material.roughness)
				Draw(*shader);//call the normal draw method
			else {

				// Prepare the mesh
				setupMesh();

				shader->Use();
				shader->setInt("albedoMap", 0);
				shader->setInt("normalMap", 1);
				shader->setInt("metallicMap", 2);
				shader->setInt("roughnessMap", 3);
				shader->setInt("aoMap", 4);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, material.albedo);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, material.normal);
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, material.metallic);
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, material.roughness);
				glActiveTexture(GL_TEXTURE4);
				glBindTexture(GL_TEXTURE_2D, material.ao);

				// Draw mesh
				glBindVertexArray(this->VAO);
				glDrawElements(GL_TRIANGLES, this->NumIndices, GL_UNSIGNED_INT, 0);
				glBindVertexArray(0);

				// Always good practice to set everything back to defaults once configured.
				for (GLuint i = 0; i < this->textures.size(); i++)
				{
					glActiveTexture(GL_TEXTURE0 + i);
					glBindTexture(GL_TEXTURE_2D, 0);
				}
			}


		}

		void setupMesh()
		{
			// Update the coordinates of the vertices
			processVertices();

			glBindVertexArray(this->VAO);
			// Load data into vertex buffers
			glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
			// A great thing about structs is that their memory layout is sequential for all its items.
			// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
			// again translates to 3/2 floats which translates to a byte array.
			glBufferData(GL_ARRAY_BUFFER, this->NumVertices * sizeof(Vertex), &this->vertices[0], GL_STREAM_DRAW);
			//glBufferData(GL_ARRAY_BUFFER, this->NumVertices * sizeof(glm::vec3), this->pTransformedVertices, GL_STREAM_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->NumIndices * sizeof(GLuint), this->pIndices, GL_STREAM_DRAW);

			// Set the vertex attribute pointers
			// Vertex Positions
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
			// Vertex Normals
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
			// Vertex Texture Coords
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

			glBindVertexArray(0);
		}

		void processVertices() {

			// Update the coordinates of the vertices
			for (int i= 0; i < NumVertices; i++) {
				this->vertices[i].Position = this->pTransformedVertices[i];
				this->vertices[i].Normal = this->pTransformedNormals[i];
			}
		}


	};

	struct sSkeleton
	{
		std::vector<sBone> Bones;
	};

	template <typename _Ty>
	struct sNodeAnimationKey
	{
		_Ty Value;
		float Time;
	};

	struct sNodeAnimation
	{
		std::string Name;

		std::vector<sNodeAnimationKey<glm::vec3> > PositionKeys;
		std::vector<sNodeAnimationKey<glm::quat> > RotationKeys;
	};

	struct sAnimation
	{
		std::vector<sNodeAnimation> NodeAnimations;

		float TicksPerSecond;
		float Duration;
	};
}