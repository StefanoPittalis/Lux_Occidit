#version 330 core

const float						PI = 3.14159265359;
const int						NR_POINT_LIGHTS = 2;

// In variables
in vec2							TexCoords;
in vec3							WorldPos;
in vec3							Normal;
in vec4							FragPosLightSpace;

// Out variables
layout (location = 0) out vec4	FragColor;
layout (location = 1) out vec4	BrightColor;

// Material parameters
uniform sampler2D				albedoMap;
uniform sampler2D				normalMap;
uniform sampler2D				metallicMap;
uniform sampler2D				roughnessMap;
uniform sampler2D				aoMap;
uniform sampler2D				shadowMap;

struct PointLight {
    vec3 position;
	vec3 direction;
    
    /*float constant;
    float linear;
    float quadratic; attenuations are already in the code*/

    vec3 diffuse;//color light
};

struct SpotLight {

    vec3 position;  
    vec3 direction;
    float cutOff;
    float outerCutOff;
	
    float constant;
    float linear;
    float quadratic;   

	vec3 diffuse;//color light
};

// Lights info
uniform PointLight					pointLights[NR_POINT_LIGHTS];
uniform SpotLight					spotLight;
uniform vec3						camPos;

// Function declarations
vec3 getNormalFromMap();
float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySchlickGGX(float NdotV, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 fresnelSchlick(float cosTheta, vec3 F0);
float ShadowCalculation(vec4 fragPosLightSpace);



void main()
{		
    vec3 albedo     = pow(texture(albedoMap, TexCoords).rgb, vec3(2.2));
    float metallic  = texture(metallicMap, TexCoords).r;
    float roughness = texture(roughnessMap, TexCoords).r;
    float ao        = texture(aoMap, TexCoords).r;

	// Compute the shadows
	float shadow = ShadowCalculation(FragPosLightSpace);

	vec3 ambient = (vec3(0.03) + (1-shadow)) * albedo * ao;

    vec3 N = getNormalFromMap();
    vec3 V = normalize(camPos - WorldPos);

    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)    
    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metallic);

    // reflectance equation
    vec3 Lo = vec3(0.0);
	//reflectance equation for point lights
    for(int i = 0; i <  NR_POINT_LIGHTS; ++i)//calc color for point lights 
    {
        // calculate per-light radiance
        vec3 L = normalize(pointLights[i].position - WorldPos);
        vec3 H = normalize(V + L);
        float distance = length(pointLights[i].position - WorldPos);
        float attenuation = 1.0 / (distance * distance);
        vec3 radiance = pointLights[i].diffuse * attenuation;

        // Cook-Torrance BRDF
        float NDF = DistributionGGX(N, H, roughness);   
        float G   = GeometrySmith(N, V, L, roughness);      
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
           
        vec3 nominator    = NDF * G * F; 
        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
        vec3 specular = nominator / denominator;
        
        // kS is equal to Fresnel
        vec3 kS = F;
        // for energy conservation, the diffuse and specular light can't
        // be above 1.0 (unless the surface emits light); to preserve this
        // relationship the diffuse component (kD) should equal 1.0 - kS.
        vec3 kD = vec3(1.0) - kS;
        // multiply kD by the inverse metalness such that only non-metals 
        // have diffuse lighting, or a linear blend if partly metal (pure metals
        // have no diffuse light).
        kD *= 1.0 - metallic;	  

        // scale light by NdotL
        float NdotL = max(dot(N, L), 0.0);        

        // add to outgoing radiance Lo
        Lo += (kD * albedo / PI + specular) * radiance * NdotL;  // note that we already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again
    } 
	
	
	//reflectance for spot light
	// calculate per-light radiance
    vec3 L = normalize(spotLight.position - WorldPos);
    vec3 H = normalize(V + L);
	vec3 lightDir = normalize(spotLight.position - WorldPos);
		
	// spotlight (soft edges)
	float theta = dot(lightDir, normalize(-spotLight.direction)); 
	float epsilon = (spotLight.cutOff - spotLight.outerCutOff);
	float intensity = clamp((theta - spotLight.outerCutOff) / epsilon, 0.0, 1.0);
    vec3 radiance = spotLight.diffuse * intensity;

	// attenuation
	float distance    = length(spotLight.position - WorldPos);
	float attenuation = 1.0 / (spotLight.constant + spotLight.linear * distance + spotLight.quadratic * (distance * distance));    
	ambient  *= attenuation; 
	radiance *= attenuation;
		   
    // Cook-Torrance BRDF
    float NDF = DistributionGGX(N, H, roughness);   
    float G   = GeometrySmith(N, V, L, roughness);      
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
           
    vec3 nominator    = NDF * G * F; 
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
    vec3 specular = nominator / denominator;
	specular *= intensity;//add the intensity to the specular component from cook torrance
    specular *= attenuation;//add the attenuation
		
    // kS is equal to Fresnel
    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - metallic;	  
    // scale light by NdotL
    float NdotL = max(dot(N, L), 0.0);        
    // add to outgoing radiance Lo
    Lo += (kD * albedo / PI + specular) * radiance * NdotL;  // note that we already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again


	// Final color
    vec3 color = (ambient + Lo);

    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // gamma correct
    color = pow(color, vec3(1.0/2.2)); 

    FragColor = vec4(color, 1.0);
	
	//add filter for blur (only for ferns)
	//float brightness = dot(color, vec3(20.0, 20.0, 20.0));
    //if(brightness > 1.0)
    //    BrightColor = vec4(color, 1.0);
    //else
        BrightColor = vec4(0.0, 0.0, 0.0, 1.0);
    
}

//// FUNCTIONS ////

vec3 getNormalFromMap()
{
    vec3 tangentNormal = texture(normalMap, TexCoords).xyz * 2.0 - 1.0;

    vec3 Q1  = dFdx(WorldPos);
    vec3 Q2  = dFdy(WorldPos);
    vec2 st1 = dFdx(TexCoords);
    vec2 st2 = dFdy(TexCoords);

    vec3 N   = normalize(Normal);
    vec3 T  = normalize(Q1*st2.t - Q2*st1.t);
    vec3 B  = -normalize(cross(N, T));
    mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}


float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

float ShadowCalculation(vec4 fragPosLightSpace)
{
// perform perspective divide
vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

// Transform to [0,1] range
projCoords = projCoords * 0.5 + 0.5;

// Get closest depth value from light�s perspective (using [0,1] range fragPosLight as coords)
float closestDepth = texture(shadowMap, projCoords.xy).r;

// Get depth of current fragment from light�s perspective
float currentDepth = projCoords.z;

// Calculate bias (based on depth map resolution and slope)
vec3 normal = normalize(Normal);
vec3 lightDir = pointLights[1].direction;
float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);

 float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;
    
    // Keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(projCoords.z > 1.0)
        shadow = 0.0;


return shadow;

}