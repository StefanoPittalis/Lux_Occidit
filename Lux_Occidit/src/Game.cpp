#include "Game.h"

// Properties
const GLuint		SCREEN_WIDTH = 1200, SCREEN_HEIGHT = 800;
const GLuint		SHADOW_WIDTH = 1024 * 3, SHADOW_HEIGHT = 1024 * 3;
const glm::vec3		POINT_THE_LIGHT_ARE_LOOKING_AT = glm::vec3(5.0f, -10.0f, 0.0f);
const GLfloat		CAM_SPEED = 5.0;
const GLfloat		PLAYER_SPEED = 5.0;

// Camera
Camera				camera(glm::vec3(6.3f, 0.0f, 1.1f));
bool				keys[1024];
GLfloat				lastX = 400, lastY = 300;
bool				firstMouse = true;

GLfloat				deltaTime = 0.0f;
GLfloat				lastFrame = 0.0f;

// Start the execution of the game
void Game::StartGame() {

	// Init GLFW
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lux Occidit", nullptr, nullptr); // Windowed
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	/* Options
	GLFW_CURSOR_NORMAL makes the cursor visible and behaving normally.
	GLFW_CURSOR_HIDDEN makes the cursor invisible when it is over the client area of the window but does not restrict the cursor from leaving.
	GLFW_CURSOR_DISABLED hides and grabs the cursor, providing virtual and unlimited cursor movement. This is useful for implementing for example 3D camera controls.
	*/
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Initialize GLEW to setup the OpenGL Function pointers
	glewExperimental = GL_TRUE;
	glewInit();

	// Define the viewport dimensions
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	// Setup some OpenGL options
	glEnable(GL_DEPTH_TEST);

	//Setup shader for blur
	FrameBufferBlur fbb;
	Shader::SetUpFrameBuffersForBlur(SCREEN_WIDTH, SCREEN_HEIGHT, &fbb);

	// Setup the framebuffer for shadowmap of the scene
	setupFramebufferForShadowmap(depthMapFBO, depthMap);

	// Load all the necessary assets
	loadShaders();
	loadModels();
	loadLights();

	// Set uniform for blur: shader configuration
	setUniformForBlur();

	// Get the params to 0
	distorsionForSSS = 6;
	scaleForSSS = 0.5;
	powerForSSS = 0.6;

	// Timer for the movement of the player
	timeValue = 0;

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Check and call events
		glfwPollEvents();

		// Update objects positions
		moveCamera();
		moveLight(&m_lights["pointLightMovable"]->modelOfLight->modelMatrix, deltaTime, CAM_SPEED);

		// Update the params for SSS
		if (modifySSSParam(&distorsionForSSS, &scaleForSSS, &powerForSSS))
			std::cout << "Distorsion: " << distorsionForSSS << ", scale: " << scaleForSSS << ", power: " << powerForSSS << "." << std::endl;

		// Clear the colorbuffer
		glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Render to depth map
		renderToDepthMap(depthMapFBO);

		// Render scene into floating point framebuffer
		glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, fbb.hdrFBO);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Setup the info of the lights for pbr
		setupShaderForPBR(m_shaders["PBR_scene_Shader"]);
		setupShaderForPBR(m_shaders["PBR_player_Shader"]);
		setupShaderForPBR(m_shaders["subSurfaceScatteringShader"]);

		// Draw the lights in the scene, with their relative model
		drawLights();

		// Draw all the models in the scene, excluding the model of the lights
		drawModels();

		// Unbind Framebufer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		//Apply blur effect to the scene
		applyBlur(fbb);

		// Swap the buffers
		glfwSwapBuffers(window);
	}

	glfwTerminate();
}

// Load assets
void Game::loadShaders()
{
	//Shader for simple light color
	m_shaders.insert(std::pair<std::string, Shader*>("lampShader", new Shader("./shader/vertex/lampShader.vertex", "./shader/fragment/lampShader.fragment")));

	//Shader for testing pbr
	m_shaders.insert(std::pair<std::string, Shader*>("PBR_scene_Shader", new Shader("./shader/vertex/PBR_scene_Shader.vertex", "./shader/fragment/PBR_scene_Shader.fragment")));
	m_shaders.insert(std::pair<std::string, Shader*>("PBR_player_Shader", new Shader("./shader/vertex/PBR_player_Shader.vertex", "./shader/fragment/PBR_player_Shader.fragment")));

	//Shaders for blur
	m_shaders.insert(std::pair<std::string, Shader*>("shaderBloom", new Shader("./shader/vertex/shaderBloom.vertex", "./shader/fragment/shaderBloom.fragment")));
	m_shaders.insert(std::pair<std::string, Shader*>("lightShader", new Shader("./shader/vertex/lightShader.vertex", "./shader/fragment/lightShader.fragment")));
	m_shaders.insert(std::pair<std::string, Shader*>("blurShader", new Shader("./shader/vertex/blurShader.vertex", "./shader/fragment/blurShader.fragment")));
	m_shaders.insert(std::pair<std::string, Shader*>("bloomFinalShader", new Shader("./shader/vertex/bloomFinalShader.vertex", "./shader/fragment/bloomFinalShader.fragment")));

	// Shader for shadow maps
	m_shaders.insert(std::pair<std::string, Shader*>("simpleDepthShader", new Shader("./shader/vertex/simpleDepthShader.vertex", "./shader/fragment/simpleDepthShader.fragment")));
	m_shaders.insert(std::pair<std::string, Shader*>("showShadowMapShader", new Shader("./shader/vertex/showShadowMapShader.vertex", "./shader/fragment/showShadowMapShader.fragment")));

	// Shader for SubSurfaceScattering
	m_shaders.insert(std::pair<std::string, Shader*>("subSurfaceScatteringShader", new Shader("./shader/vertex/subSurfaceScatteringShader.vertex", "./shader/fragment/subSurfaceScatteringShader.fragment")));
}
void Game::loadModels()
{
	// Load player
	glm::mat4 playerInitialPosition = glm::mat4();
	playerInitialPosition = glm::translate(playerInitialPosition, glm::vec3(1.0f, -5.0f, 0.0f));
	playerInitialPosition = glm::scale(playerInitialPosition, glm::vec3(0.2f, 0.2f, 0.2f));
	playerInitialPosition = glm::rotate(playerInitialPosition, (glm::mediump_float)4.74, glm::vec3(1.0f, 0.0f, 0.0f));
	m_models.insert(std::pair<std::string, Model*>("playerModel", new Model("./models/player/player.dae", playerInitialPosition)));
	m_models["playerModel"]->SetUpMaterialAnimation("./models/player/Albedo_bake.png", "./models/player/Normal_bake.png", "./models/player/Metallic_bake.png", "./models/player/Roughness_bake.png", "./models/player/AO_bake.png");

	// Load cave
	m_models.insert(std::pair<std::string, Model*>("caveModel", new Model("./models/cave/simple_cave.dae")));
	m_models["caveModel"]->SetUpMaterial("./models/cave/rock_sliced_Base_Color.png", "./models/cave/rock_sliced_Normal.png", "./models/cave/rock_sliced_Metallic.png", "./models/cave/rock_sliced_Roughness.png", "./models/cave/rock_sliced_Ambient_Occlusion.png");

	// Load fern
	m_models.insert(std::pair<std::string, Model*>("fernModel", new Model("./models/fern/fern.dae")));

	// Load crystal
	m_models.insert(std::pair<std::string, Model*>("crystalModel", new Model("./models/crystal/crystal.dae")));
	m_models["crystalModel"]->SetUpMaterial("./models/crystal/crystal-albedo.png", "./models/crystal/crystal-normal.png", "./models/crystal/crystal-metal.png", "./models/crystal/crystal-roughness.png", "./models/crystal/AO-crystal.png", "./models/crystal/crystal-thickness.png");
}
void Game::loadLights()
{
	// Positions of the pointLights
	glm::mat4 modelMatrix;
	// Light 1
	modelMatrix = glm::mat4();
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.5f, -5.0f, 5.5f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(1));
	m_lights.insert(std::pair<std::string, Light*>("pointLightStatic", new Light("./models/mushroom/mushroom.dae", pointLight, modelMatrix)));
	// Light 2
	modelMatrix = glm::mat4();
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-2.3f, -3.3f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2));
	m_lights.insert(std::pair<std::string, Light*>("pointLightMovable", new Light("./models/lights/light_sphere.dae", pointLight, modelMatrix)));

	// Position and direction for spotLights
	modelMatrix = glm::mat4();
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -15.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2));
	m_lights.insert(std::pair<std::string, Light*>("spotLight", new Light("./models/lights/light_sphere.dae", spotLight, modelMatrix, glm::vec3(0.0f, -1.0f, 0.0f))));
}

// Draw Models
void Game::drawLights()
{
	// Create standard matrixes
	glm::mat4 projection;
	glm::mat4 view;

	projection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
	view = camera.GetViewMatrix();

	// Iterate on the lights in the scene
	for (auto const& light : m_lights) {

		// Draw pointLights
		if (light.second->typeOfLight == pointLight)
		{
			m_shaders["lampShader"]->Use();
			glUniformMatrix4fv(glGetUniformLocation(m_shaders["lampShader"]->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));//use the old matrix
			glUniformMatrix4fv(glGetUniformLocation(m_shaders["lampShader"]->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));//use the old matrix
			m_shaders["lampShader"]->setMat4("model", light.second->modelOfLight->modelMatrix);
			m_shaders["lampShader"]->setVec3("lightColor", glm::vec3(1.0, 1.0, 1.0));
			m_shaders["lampShader"]->setVec3("objectColor", glm::vec3(1.0, 1.0, 1.0));
			light.second->modelOfLight->Draw(m_shaders["lampShader"]);
		}

		// Draw spot lights
		else if (light.second->typeOfLight == spotLight) {

			m_shaders["lampShader"]->Use();
			glUniformMatrix4fv(glGetUniformLocation(m_shaders["lampShader"]->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));//use the old matrix
			glUniformMatrix4fv(glGetUniformLocation(m_shaders["lampShader"]->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));//use the old matrix
			// Crete a new vec3 to push the light a little higher
			glm::mat4 modelMatrix = glm::translate(light.second->modelOfLight->modelMatrix, glm::vec3(0.0f, 95.0f, 0.0f));
			m_shaders["lampShader"]->setMat4("model", modelMatrix);
			m_shaders["lampShader"]->setVec3("lightColor", glm::vec3(1.0, 1.0, 1.0));
			m_shaders["lampShader"]->setVec3("objectColor", glm::vec3(1.0, 1.0, 1.0));
			light.second->modelOfLight->Draw(m_shaders["lampShader"]);
		}
	}
}
void Game::drawModels(bool isShadowmapRendering)
{
	// Create standard matrixes
	glm::mat4 projection;
	glm::mat4 view;
	glm::mat4 model;
	GLfloat near_plane = 1.0f, far_plane = 7.5f;
	glm::mat4 lightProjection = glm::ortho(-30.0f, 30.0f, -30.0f, 30.0f, near_plane, far_plane);
	/*glm::mat4 lightProjection= glm::perspective(camera.Zoom, (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT, 0.1f, 100.0f);*/
	glm::mat4 lightView = glm::lookAt(m_lights["pointLightMovable"]->getPosition(), POINT_THE_LIGHT_ARE_LOOKING_AT, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 lightSpaceMatrix = lightProjection * lightView;

	// Shader use for rendering the model
	Shader* modelShader;

	/*	Draw the objects in the scene	*/
	drawCrystal(isShadowmapRendering, modelShader, lightSpaceMatrix, projection, view, model);
	drawCave(isShadowmapRendering, modelShader, lightSpaceMatrix, projection, view, model);
	drawPlayer(isShadowmapRendering, modelShader, lightSpaceMatrix, projection, view);
	drawFern(isShadowmapRendering, modelShader, projection, view, model);
}

// Draw specific models
void Game::drawFern(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &projection, glm::mat4 &view, glm::mat4 &model)
{
	if (isShadowmapRendering) {
		modelShader = m_shaders["simpleDepthShader"];
		modelShader->Use();   // <-- Don't forget this one!

		glm::mat4 lightProjection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		glm::mat4 lightView = glm::lookAt(glm::vec3(2.3f, -3.3f, -4.0f), glm::vec3(0.0f), glm::vec3(1.0));
		glm::mat4 lightSpaceMatrix = lightProjection * lightView;
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
	}
	else {
		modelShader = m_shaders["lightShader"];

		float timeValue = glfwGetTime();
		float greenValue = sin(timeValue);
		modelShader->Use();
		modelShader->setMat4("projection", projection);
		modelShader->setMat4("view", view);
		modelShader->setFloat("greenValue", greenValue);

		// Transformation matrices
		projection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	}

	// Draw the loaded model
	// Fern 1
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(3.0f, -5.0f, 6.0f)); // Translate it down a bit so it's at the center of the scene
	model = glm::scale(model, glm::vec3(1.8f));	// It's a bit too big for our scene, so scale it down
	modelShader->setMat4("model", model);

	m_models["fernModel"]->Draw(modelShader);

	// Fern 2
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(3.0f, -5.0f, -4.9f)); // Translate it down a bit so it's at the center of the scene
	model = glm::scale(model, glm::vec3(1.2f));	// It's a bit too big for our scene, so scale it down
	m_shaders["lightShader"]->setMat4("model", model);
	m_models["fernModel"]->Draw(modelShader);

	// Fern 3
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-2.0f, -5.0f, -2.5f)); // Translate it down a bit so it's at the center of the scene
	model = glm::scale(model, glm::vec3(2.2f));	// It's a bit too big for our scene, so scale it down
	m_shaders["lightShader"]->setMat4("model", model);
	m_models["fernModel"]->Draw(modelShader);

	// Fern 4
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-2.0f, -5.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	model = glm::scale(model, glm::vec3(1.8f));	// It's a bit too big for our scene, so scale it down
	m_shaders["lightShader"]->setMat4("model", model);
	m_models["fernModel"]->Draw(modelShader);

}
void Game::drawCrystal(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &lightSpaceMatrix, glm::mat4 &projection, glm::mat4 &view, glm::mat4 &model)
{
#pragma region Crystal 1
	if (isShadowmapRendering)
		modelShader = m_shaders["simpleDepthShader"];
	else
		modelShader = m_shaders["subSurfaceScatteringShader"];

	modelShader->Use();   
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

	if (!isShadowmapRendering) {
		// Transformation matrices
		projection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		modelShader->setVec3("camPos", camera.Position);
		modelShader->setVec3("pointLights[1].position", m_lights["pointLightMovable"]->getPosition());

		// Variables for SSS
		modelShader->setFloat("distorsion", distorsionForSSS);
		modelShader->setFloat("scale", scaleForSSS);
		modelShader->setFloat("power", powerForSSS);

		// Add the sample texture for the shadows
		glActiveTexture(GL_TEXTURE5);
		glUniform1i(glGetUniformLocation(modelShader->Program, "shadowMap"), 5);
		glBindTexture(GL_TEXTURE_2D, depthMap);

	}

	// Draw the loaded model
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-0.5f, -5.0f, -5.0f)); 
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
	m_models["crystalModel"]->DrawPBR(modelShader);
#pragma endregion

#pragma region Crystal2
	if (isShadowmapRendering)
		modelShader = m_shaders["simpleDepthShader"];
	else
		modelShader = m_shaders["subSurfaceScatteringShader"];

	modelShader->Use();   
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

	if (!isShadowmapRendering) {
		// Transformation matrices
		projection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		modelShader->setVec3("camPos", camera.Position);
		modelShader->setVec3("pointLights[1].position", m_lights["pointLightMovable"]->getPosition());

		// Variables for SSS
		modelShader->setFloat("distorsion", distorsionForSSS);
		modelShader->setFloat("scale", scaleForSSS);
		modelShader->setFloat("power", powerForSSS);

		// Add the sample texture for the shadows
		glActiveTexture(GL_TEXTURE5);
		glUniform1i(glGetUniformLocation(modelShader->Program, "shadowMap"), 5);
		glBindTexture(GL_TEXTURE_2D, depthMap);

	}

	// Draw the loaded model
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-1.3f, -5.0f, 5.5f)); 
	model = glm::scale(model, glm::vec3(1.1)); 
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
	m_models["crystalModel"]->DrawPBR(modelShader);
#pragma endregion

#pragma region Crystal2
	if (isShadowmapRendering)
		modelShader = m_shaders["simpleDepthShader"];
	else
		modelShader = m_shaders["subSurfaceScatteringShader"];

	modelShader->Use();   
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

	if (!isShadowmapRendering) {
		// Transformation matrices
		projection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		modelShader->setVec3("camPos", camera.Position);
		modelShader->setVec3("pointLights[1].position", m_lights["pointLightMovable"]->getPosition());

		// Variables for SSS
		modelShader->setFloat("distorsion", distorsionForSSS);
		modelShader->setFloat("scale", scaleForSSS);
		modelShader->setFloat("power", powerForSSS);

		// Add the sample texture for the shadows
		glActiveTexture(GL_TEXTURE5);
		glUniform1i(glGetUniformLocation(modelShader->Program, "shadowMap"), 5);
		glBindTexture(GL_TEXTURE_2D, depthMap);

	}

	// Draw the loaded model
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(0.5f, -5.0f, -6.5f));
	model = glm::scale(model, glm::vec3(0.5));
	model = glm::rotate(model, (glm::mediump_float)1.58, glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
	m_models["crystalModel"]->DrawPBR(modelShader);
#pragma endregion
}
void Game::drawPlayer(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &lightSpaceMatrix, glm::mat4 &projection, glm::mat4 &view)
{
	if (isShadowmapRendering)
		modelShader = m_shaders["simpleDepthShader"];
	else
		modelShader = m_shaders["PBR_player_Shader"];

	modelShader->Use();   

	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

	if (!isShadowmapRendering) {
		// Transformation matrices
		projection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		modelShader->setVec3("pointLights[1].position", m_lights["pointLightMovable"]->getPosition());
		modelShader->setVec3("camPos", camera.Position);

		// Add the sample texture for the shadows
		glActiveTexture(GL_TEXTURE5);
		glUniform1i(glGetUniformLocation(modelShader->Program, "shadowMap"), 5);
		glBindTexture(GL_TEXTURE_2D, depthMap);

	}

	// Draw the loaded model
	glm::mat4 modelPlayer = m_models["playerModel"]->modelMatrix;
	timeValue += 0.1 * deltaTime;
	float movementValueY = sin(timeValue);
	float movementValueX = cos(timeValue);
	modelPlayer = glm::translate(modelPlayer, glm::vec3(-10.0f* movementValueX, -25.0f * movementValueY, 0.0f));	// Move the player in a circular fashion
	modelPlayer = glm::rotate(modelPlayer, (glm::mediump_float)timeValue, glm::vec3(0.0f, 0.0f, 1.0f));				// Make the rotation of the player coherent with the path
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelPlayer));

	m_models["playerModel"]->PlayAnimation(*modelShader, deltaTime);
}
void Game::drawCave(bool isShadowmapRendering, Shader * &modelShader, glm::mat4 &lightSpaceMatrix, glm::mat4 &projection, glm::mat4 &view, glm::mat4 &model)
{
	if (isShadowmapRendering)
		modelShader = m_shaders["simpleDepthShader"];
	else
		modelShader = m_shaders["PBR_scene_Shader"];

	modelShader->Use();   
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

	if (!isShadowmapRendering) {
		// Transformation matrices
		projection = glm::perspective(camera.Zoom, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
		view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		modelShader->setVec3("pointLights[1].position", m_lights["pointLightMovable"]->getPosition());
		modelShader->setVec3("camPos", camera.Position);

		// Add the sample texture for the shadows
		glActiveTexture(GL_TEXTURE5);
		glUniform1i(glGetUniformLocation(modelShader->Program, "shadowMap"), 5);
		glBindTexture(GL_TEXTURE_2D, depthMap);

	}

	// Draw the loaded model
	model = glm::mat4();
	model = glm::translate(model, glm::vec3(0.0f, -5.0f, 0.0f)); // Translate it down a bit so it's at the center of the scene
																 //model = glm::rotate(model, (glm::mediump_float)4.74, glm::vec3(1.0f, 0.0f, 0.0f)); // Rotate to vertical position, radians 
	glUniformMatrix4fv(glGetUniformLocation(modelShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
	m_models["caveModel"]->DrawPBR(modelShader);
}

// PBR: Setup the info of the lights
void Game::setupShaderForPBR(Shader* shader)
{
	shader->Use();   
	shader->setVec3("camPos", camera.Position);

	// Spot light 
	shader->setVec3("spotLight.position", m_lights["spotLight"]->getPosition());
	shader->setFloat("spotLight.constant", 1.0f);
	shader->setFloat("spotLight.linear", 0.09f);
	shader->setFloat("spotLight.quadratic", 0.032f);
	shader->setVec3("spotLight.direction", -m_lights["spotLight"]->direction);
	shader->setFloat("spotLight.cutOff", glm::cos(glm::radians(10.5f)));
	shader->setFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.5f)));
	shader->setVec3("spotLight.diffuse", glm::vec3(40.0f));

	// Point light 1 
	shader->setVec3("pointLights[0].position", m_lights["pointLightStatic"]->getPosition());
	shader->setVec3("pointLights[0].direction", m_lights["pointLightStatic"]->getPosition());
	shader->setVec3("pointLights[0].diffuse", glm::vec3(5.0f));
	// Point light 2
	shader->setVec3("pointLights[1].position", m_lights["pointLightMovable"]->getPosition());
	shader->setVec3("pointLights[1].direction", POINT_THE_LIGHT_ARE_LOOKING_AT - m_lights["pointLightMovable"]->getPosition());
	
	shader->setVec3("pointLights[1].diffuse", glm::vec3(10.0f));
}

// Blur Effect
void Game::setUniformForBlur()
{
	m_shaders["shaderBloom"]->Use();
	m_shaders["shaderBloom"]->setInt("diffuseTexture", 0);
	m_shaders["blurShader"]->Use();
	m_shaders["blurShader"]->setInt("image", 0);
	m_shaders["bloomFinalShader"]->Use();
	m_shaders["bloomFinalShader"]->setInt("scene", 0);
	m_shaders["bloomFinalShader"]->setInt("bloomBlur", 1);
}
void Game::applyBlur(FrameBufferBlur &fbb)
{
	// Blur bright fragments with two-pass Gaussian Blur 
	bool horizontal = true, first_iteration = true;
	unsigned int amount = 50;

	m_shaders["blurShader"]->Use();

	for (unsigned int i = 0; i < amount; i++)//if we add more iteration we obtain a more pretty blur effect but more expensive!
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbb.pingpongFBO[horizontal]);
		m_shaders["blurShader"]->setInt("horizontal", horizontal);
		glBindTexture(GL_TEXTURE_2D, first_iteration ? fbb.colorBuffers[1] : fbb.pingpongColorbuffers[!horizontal]);  // bind texture of other framebuffer (or scene if first iteration)
		Shader::renderQuad(&fbb);
		horizontal = !horizontal;
		if (first_iteration)
			first_iteration = false;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	m_shaders["bloomFinalShader"]->Use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fbb.colorBuffers[0]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, fbb.pingpongColorbuffers[!horizontal]);
	m_shaders["bloomFinalShader"]->setInt("bloom", true);
	m_shaders["bloomFinalShader"]->setFloat("exposure", 0.5f);
	Shader::renderQuad(&fbb);
}

// Shadowmap
void Game::setupFramebufferForShadowmap(GLuint& depthMapFBO, GLuint& depthMap)
{
	// depthMap for all meshes in scene
	glGenFramebuffers(1, &depthMapFBO);

	// First we�ll create a framebuffer object for rendering the depth map
	glGenTextures(1, &depthMap);

	// Next we create a 2D texture that we�ll use as the framebuffer�s depth buffer:
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);


	GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	//vWith the generated depth texture we can attach it as the framebuffer�s depth buffer:
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}
void Game::renderToDepthMap(const GLuint &depthMapFBO)
{
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_FRONT);
	drawModels(true);
	glCullFace(GL_BACK);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

// Callback functions
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos;

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

// Movement
void moveCamera()
{
	// Camera controls
	if (keys[GLFW_KEY_W])
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (keys[GLFW_KEY_S])
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (keys[GLFW_KEY_A])
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (keys[GLFW_KEY_D])
		camera.ProcessKeyboard(RIGHT, deltaTime);
}
void moveLight(glm::mat4* light_position, GLfloat deltaTime, GLfloat speedness) {

	GLfloat velocity = speedness * deltaTime;
	if (keys[GLFW_KEY_M])//change here for input
		*light_position = (glm::translate(*light_position, glm::vec3(1.0f, 0.0f, 0.0f) * velocity));
	if (keys[GLFW_KEY_I])//change here for input
		*light_position = (glm::translate(*light_position, glm::vec3(-1.0f, 0.0f, 0.0f) * velocity));
	if (keys[GLFW_KEY_J])//change here for input
		*light_position = (glm::translate(*light_position, glm::vec3(0.0f, 0.0f, 1.0f) * velocity));
	if (keys[GLFW_KEY_K])//change here for input
		*light_position = (glm::translate(*light_position, glm::vec3(0.0f, 0.0f, -1.0f) * velocity));
	if (keys[GLFW_KEY_O]) //change here for input
		*light_position = (glm::translate(*light_position, glm::vec3(0.0f, 1.0f, 0.0f) * velocity));
	if (keys[GLFW_KEY_P]) //change here for input
		*light_position = (glm::translate(*light_position, glm::vec3(0.0f, -1.0f, 0.0f) * velocity));
}

// Change the params for SSS
bool modifySSSParam(float* distorsionForSSS, float* scaleForSSS, float* powerForSSS) {
	// Change distorsionForSSS
	bool modified = false;
	if (keys[GLFW_KEY_Z]) {
		*distorsionForSSS += 0.1;
		modified = true;
	}
	if (keys[GLFW_KEY_V]) {
		*distorsionForSSS -= 0.1;
		modified = true;
	}
	// Change scaleForSSS
	if (keys[GLFW_KEY_X]) {
		*scaleForSSS += 0.1;
		modified = true;
	}
	if (keys[GLFW_KEY_B]) {
		*scaleForSSS -= 0.1;
		modified = true;
	}
	// Change powerForSSS
	if (keys[GLFW_KEY_C]) {
		*powerForSSS += 0.1;
		modified = true;
	}
	if (keys[GLFW_KEY_N]) {
		*powerForSSS -= 0.1;
		modified = true;
	}
	return modified;
};