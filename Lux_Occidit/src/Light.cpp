#include "Light.h"

Light::Light(GLchar* path, light_types typeOfLight, glm::mat4 homogeneousMatrix, glm::vec3 direction) {
	this->modelOfLight = new Model(path, homogeneousMatrix);
	this->typeOfLight = typeOfLight;
	this->direction = direction;
};

// Real position of the light source
glm::vec3	Light::getPosition() {
	return glm::vec3(modelOfLight->modelMatrix[3][0], modelOfLight->modelMatrix[3][1], modelOfLight->modelMatrix[3][2]);
};