#include "Model.h"

/*  Public Functions   */
// Constructor, expects a filepath to a 3D model.
Model::Model(GLchar* path, glm::mat4 position)
{
	this->loadModel(path);
	this-> modelMatrix = position;
}

// Draws the model, and thus all its meshes
void Model::Draw(Shader* shader)
{
	for (GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw(shader);
}

// Draws the model, and thus all its meshes
void Model::DrawPBR(Shader* shader)
{
	for (GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].DrawPBR(shader);
}

// Play the animation, drawing the model
void Model::PlayAnimation(Shader shader, GLfloat deltaTime)
{
	// Update the animation
	this->AnimatedModel.Update(deltaTime);

	for (GLuint i = 0; i < this->AnimatedModel.m_Meshes.size(); i++) {
		this->AnimatedModel.m_Meshes[i].DrawPBR(&shader);
	}
}


/*  Private Functions   */
// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
void Model::loadModel(std::string path)
{
	// Read file via ASSIMP
	scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
	// Check for errors
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
	{
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
		return;
	}
	// Retrieve the directory path of the filepath
	this->directory = path.substr(0, path.find_last_of('/'));

	// Process ASSIMP's root node recursively
	this->processNode(scene->mRootNode, scene);

	// Try to convert SkeletalModel
	AssimpConverter::Convert(scene, AnimatedModel);

	// Pass the texture to the SkeletalModel
	for (int i = 0; i < AnimatedModel.m_Meshes.size(); i++) {
		this->AnimatedModel.m_Meshes[i].vertices = this->meshes[i].vertices;
		this->AnimatedModel.m_Meshes[i].textures = this->meshes[i].textures;
	}
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void Model::processNode(aiNode* node, const aiScene* scene)
{
	// Process each mesh located at the current node
	for (GLuint i = 0; i < node->mNumMeshes; i++)
	{
		// The node object only contains indices to index the actual objects in the scene. 
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		this->meshes.push_back(this->processMesh(mesh, scene));
	}
	// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		this->processNode(node->mChildren[i], scene);
	}

}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
	// Data to fill
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	// Walk through each of the mesh's vertices
	for (GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		// IMPORTANT: y and z are flipped because we are using model made in blender!!!
		Vertex vertex;
		glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
						  // Positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].z;
		vector.z = mesh->mVertices[i].y;
		vertex.Position = vector;
		// Normals
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].z;
		vector.z = mesh->mNormals[i].y;
		vertex.Normal = vector;
		// Texture Coordinates
		if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		vertices.push_back(vertex);
	}
	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for (GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices vector
		for (GLuint j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}

	// Process materials
	if (mesh->mMaterialIndex >= 0)
	{
		std::cout << "Materiale numero: " << mesh->mMaterialIndex << ".\n";
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: texture_diffuseN
		// Specular: texture_specularN
		// Normal: texture_normalN

		// 1. Diffuse maps
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

		std::cout << "Diffuse trovate: " << diffuseMaps.size() << ".\n";
		std::cout << "Specular trovate: " << specularMaps.size() << ".\n";
	}

	std::cout << "=====================\n";

	// Return a mesh object created from the extracted mesh data
	return Mesh(vertices, indices, textures, mesh);
}

// Checks all material textures of a given type and loads the textures if they're not loaded yet.
// The required info is returned as a Texture struct.
std::vector<Texture> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (GLuint i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		std::cout << "Texture associata: " << str.data << "\n";
		// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
		GLboolean skip = false;
		for (GLuint j = 0; j < textures_loaded.size(); j++)
		{
			if (std::strcmp(textures_loaded[j].path.C_Str(), str.C_Str()) == 0)
			{
				textures.push_back(textures_loaded[j]);
				skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
				break;
			}
		}
		if (!skip)
		{   // If texture hasn't been loaded already, load it
			Texture texture;
			texture.id = TextureFromFile(str.C_Str(), this->directory);
			texture.type = typeName;
			texture.path = str;
			textures.push_back(texture);
			this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
		}
	}
	return textures;
}

GLint TextureFromFile(const char* path, std::string directory)
{
	//Generate texture ID and load texture data 
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width, height, nrComponents;
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, &nrComponents, SOIL_LOAD_AUTO);
	//SOIL_LOAD_RGBA
	// Assign texture to ID
	GLenum format = GL_RGB;
	if (nrComponents == 1)
		format = GL_RED;
	else if (nrComponents == 3)
		format = GL_RGB;
	else if (nrComponents == 4)
		format = GL_RGBA;

	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);
	return textureID;
}

GLint TextureFromFile(std::string path)
{
	std::string filename;
	//filename = FileSystem::getPath(path).c_str();
	filename = std::string(path);
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width, height;
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
	if (image) {
		// Assign texture to ID
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);

		// Parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		SOIL_free_image_data(image);
		std::cout << "Texture found to load at path: " << path << std::endl;
	}
	else {

		std::cout << "Texture failed to load at path: " << path << std::endl;
		SOIL_free_image_data(image);

	}
	return textureID;

}

//Fill the material field with correct special textures for cook torrance
void Model::SetUpMaterial(std::string albedo_path, std::string normal_path, std::string metallic_path, std::string roughness_path, std::string ao_path, std::string thickness_path) {

	// load PBR material textures
	// --------------------------
	material.albedo = TextureFromFile(albedo_path);
	material.normal = TextureFromFile(normal_path);
	material.metallic = TextureFromFile(metallic_path);
	material.roughness = TextureFromFile(roughness_path);
	material.ao = TextureFromFile(ao_path);

	if (thickness_path != "")
		material.thickness = TextureFromFile(thickness_path);

	//fill the mesh material with the SAME material
	for (int i = 0; i < meshes.size(); ++i)
		meshes[i].material = material;
}

//Fill the material field with correct special textures for cook torrance
void Model::SetUpMaterialAnimation(std::string albedo_path, std::string normal_path, std::string metallic_path, std::string roughness_path, std::string ao_path) {

	// load PBR material textures
	// --------------------------
	material.albedo = TextureFromFile(albedo_path);
	material.normal = TextureFromFile(normal_path);
	material.metallic = TextureFromFile(metallic_path);
	material.roughness = TextureFromFile(roughness_path);
	material.ao = TextureFromFile(ao_path);

	//fill the mesh material with the SAME material
	for (int i = 0; i < AnimatedModel.m_Meshes.size(); ++i)
		AnimatedModel.m_Meshes[i].material = material;
}

