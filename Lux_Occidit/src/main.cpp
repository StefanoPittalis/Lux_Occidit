﻿#include "Game.h"

 //The MAIN function
int main()
{
	// Create a new instance of the game
	Game* mainGame = new Game();

	// Start the game
	mainGame->StartGame();

	return 0;
}