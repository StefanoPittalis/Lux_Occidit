#Progetto Lux Occidit

#####											Corso: _Programmazione Grafica per il Tempo Reale_	Anno accademico: _2017/18_	Componenti: _Alice De Girolamo, Stefano Pittalis_

##Introduzione

L'obbiettivo che ci siamo proposti per questo progetto è stata la realizzazione di una scena di showcase in cui mostrare alcune tecniche avanzate apprese durante il corso, e alcune frutto di ulteriori approfondimenti personali. Per questo motivo la scelta dei componenti presenti in scena è stata fatta per mostrare un buon numero di tecniche, pur mantenimento un insieme organico e coerente.

![cena_](./Relazione/img/scena_1.png)

